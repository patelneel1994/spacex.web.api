using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SpaceX.Web.API.Business.Interfaces
{
    public interface IService<M, T>
    {
        M GetSingle(Expression<Func<T, bool>> whereCondition);

        void Add(M entity);

        void Delete(M entity);

        void Update(M entity);

		Task<List<M>> GetAll(Func<T, bool> predicate);

        Task<List<M>> GetAllAsync();

		Task<IQueryable<T>> Query(Expression<Func<T, bool>> whereCondition);
    
        long Count(Expression<Func<T, bool>> whereCondition);

        long Count();
    }
}