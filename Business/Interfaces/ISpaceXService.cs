using SpaceX.Web.API.Business.Models;
using System.Collections.Generic;

namespace SpaceX.Web.API.Business.Interfaces
{
	public interface ISpaceXService : IService<Launchpad, Launchpad>
    {
        List<Launchpad> GetAll();

    }
}