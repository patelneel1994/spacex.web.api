using Newtonsoft.Json;

namespace SpaceX.Web.API.Business.Models
{
    public class Launchpad
    {
        // o Launchpad Id(maps to the “id” field from the SpaceX API)
        [JsonProperty("id")]
        public string Id { get; set; }

        // o Launchpad Name(maps to the “full_name” field from the SpaceX API)
        [JsonProperty("full_name")]
        public string Name { get; set; }

        // o Launchpad Status(maps to the “status” field from the SpaceX API)
        [JsonProperty("status")]
        public string Status { get; set; }

    }
    public class LaunchpadDataRequest : Launchpad
    {
        public bool HadData => !string.IsNullOrEmpty(this.Id) ||
                                !string.IsNullOrEmpty(this.Name) ||
                                !string.IsNullOrEmpty(this.Status);

    }


}