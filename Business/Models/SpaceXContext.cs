﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using SpaceX.Web.API.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpaceX.Web.API.Business.Models
{
    public class SpaceXContext : DbContext
    {
        private const string SettingFileName = "appsettings.json";
        private const string ConnectionStringName = "DatabaseConnection";
        //public SpaceXContext(DbContextOptions<SpaceXContext> options)
        //    : base(options)
        //{
        //}

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                var configuration = new ConfigurationBuilder().AddJsonFile(SettingFileName.ToApplicationPath(), false, true).Build();
                optionsBuilder.UseSqlServer(configuration.GetConnectionString(ConnectionStringName));
            }
        }

        public DbSet<Launchpad> Launchpads { get; set; }
    }

}
