﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using NLog;
using NLog.Web;
using SpaceX.Web.API.Business.Interfaces;
using SpaceX.Web.API.Business.Models;

namespace SpaceX.Web.API.Business.Services
{
    /// <summary>
    /// This service is specifically used to call SpaceXDB
    /// </summary>
    public class SpaceXDBService : ISpaceXService
    {
        private SpaceXContext _context;
        private ILogger<SpaceXDBService> _logger;
        public SpaceXDBService(SpaceXContext context, ILogger<SpaceXDBService> logger)
        {
            _context = context;
            _logger = logger;
        }


        public void Add(Launchpad entity)
        {
            throw new NotImplementedException();
        }

        public long Count(Func<Launchpad, bool> whereCondition)
        {
            throw new NotImplementedException();
        }

        public long Count()
        {
            try
            {
                return _context.Launchpads.Count();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return -1;
        }


        public List<Launchpad> GetAll()
        {
            try
            {
                return _context.Launchpads.ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        public Task<List<Launchpad>> GetAllAsync()
        {
            try
            {
                return _context.Launchpads.ToListAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        public Launchpad GetSingle(Expression<Func<Launchpad, bool>> whereCondition)
        {
            throw new NotImplementedException();
        }

        public Task<IQueryable<Launchpad>> Query(Expression<Func<Launchpad, bool>> whereCondition)
        {
            throw new NotImplementedException();
        }

        public void Update(Launchpad entity)
        {
            throw new NotImplementedException();
        }
        public long Count(Expression<Func<Launchpad, bool>> whereCondition)
        {
            throw new NotImplementedException();
        }

        public void Delete(Launchpad entity)
        {
            throw new NotImplementedException();
        }

        public Task<List<Launchpad>> GetAll(Expression<Func<Launchpad, bool>> whereCondition)
        {
            throw new NotImplementedException();
        }

        public Task<List<Launchpad>> GetAll(Func<Launchpad, bool> predicate)
        {
            throw new NotImplementedException();
        }

    }
}
