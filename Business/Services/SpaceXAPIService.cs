using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SpaceX.Web.API.Business.Interfaces;
using SpaceX.Web.API.Business.Models;

namespace SpaceX.Web.API.Business
{
    /// <summary>
    /// This service is specifically used to call SpaceXAPI
    /// </summary>
    public class SpaceXAPIService : ISpaceXService
    {
        private static string LaunchPadEndpoint = "";
        private static string SpaceXAPI = "";
        private static HttpClient client = new HttpClient();
        private ILogger<SpaceXAPIService> _logger;


        public SpaceXAPIService(IConfiguration Configuration,ILogger<SpaceXAPIService> logger)
        {
            LaunchPadEndpoint = Configuration["LaunchPadEndpoint"];
            SpaceXAPI = Configuration["LaunchPadEndpoint"];
            _logger = logger;
        }


        private async Task<List<Launchpad>> GetAllLaunchPads()
        {
            using (HttpResponseMessage res = await client.GetAsync(LaunchPadEndpoint))
            using (HttpContent content = res.Content)
            {
                string data = await content.ReadAsStringAsync();
                if (!string.IsNullOrEmpty(data))
                {
                    return (System.Collections.Generic.List<Launchpad>)JsonConvert.DeserializeObject<IEnumerable<Launchpad>>(data); ;
                }
            }
            return null;
        }


        public async Task<List<Launchpad>> GetAll(Func<Launchpad, bool> predicate)
        {
            try
            {
                var data = await GetAllLaunchPads();
                return data.Where(predicate).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<List<Launchpad>> GetAllAsync()
        {
            try
            {
                return await GetAllLaunchPads();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public List<Launchpad> GetAll()
        {
            var res = Task.Run(async () => await client.GetAsync(LaunchPadEndpoint)).Result;
            using (HttpContent content = res.Content)
            {
                string data = Task.Run(async () => await content.ReadAsStringAsync()).Result;
                if (!string.IsNullOrEmpty(data))
                {
                    return JsonConvert.DeserializeObject<List<Launchpad>>(data);
                }
            }
            return null;
        }

        public Launchpad GetSingle(Expression<Func<Launchpad, bool>> whereCondition)
        {
            throw new NotImplementedException();
        }

        public Task<IQueryable<Launchpad>> Query(Expression<Func<Launchpad, bool>> whereCondition)
        {
            throw new NotImplementedException();
        }

        public void Update(Launchpad entity)
        {
            throw new NotImplementedException();
        }

        public void Add(Launchpad entity)
        {
            throw new NotImplementedException();
        }

        public long Count(Expression<Func<Launchpad, bool>> whereCondition)
        {
            throw new NotImplementedException();
        }

        public long Count()
        {
            throw new NotImplementedException();
        }

        public void Delete(Launchpad entity)
        {
            throw new NotImplementedException();
        }
    }
}