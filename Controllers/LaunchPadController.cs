using Microsoft.AspNetCore.Mvc;
using SpaceX.Web.API.Business.Interfaces;
using SpaceX.Web.API.Business.Models;
using System.Linq;
using System.Threading.Tasks;

namespace SpaceX.Web.API.Controllers
{
    [Route("api/[controller]")]    
    public class LaunchPadController : Controller
    {

		private ISpaceXService _apiService;
		public LaunchPadController(ISpaceXService apiService)
		{
			_apiService = apiService;
        }


        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
			var result = await _apiService.GetAllAsync();
            //We could also restrict the result to be top 50 or something 
            //and also use Paging
            if (result.Any())
            {
                return Ok(result);
            }
            return NotFound("No launchpads found!");

        }


        [HttpGet("{id}")]
		public async Task<IActionResult> Get(string id)
        {
            if (string.IsNullOrEmpty(id)) { return BadRequest("Bad paremeter provided!");}
			var result = await _apiService.GetAll(i => i.Id.Equals(id));
            if (result.FirstOrDefault() != null)
            {
				return Ok(result.First());
            }
			return NotFound($"Requested Launchpad with ID: {id} not found!");
        }
        
        [Route("name/{name}")]
        [HttpGet]
        public async Task<IActionResult> GetByName(string name)
        {
            if (string.IsNullOrEmpty(name)) { return BadRequest("Bad paremeter provided!"); }
            // I used contains which works as LIKE 
            // I could have used i.Name.Equals
            var result = await _apiService.GetAll(i => i.Name.Contains(name));

            if (result != null)
            {
                return Ok(result);
            }
            return NotFound($"Requested Launchpad with Name: {name} not found!");
        }

        [Route("status/{status}")]
        [HttpGet]
        public async Task<IActionResult> GetByStatus(string status)
        {
            if (string.IsNullOrEmpty(status)) { return BadRequest("Bad paremeter provided!"); }
            var result = await _apiService.GetAll(i => i.Status.Equals(status));

            if (result != null)
            {
                return Ok(result);
            }
            return NotFound($"Requested Launchpad with Status: {status} not found!");
        }


        [HttpPost]
		public IActionResult Post([FromBody]LaunchpadDataRequest value)
        {
            if (!value.HadData) return BadRequest();
            _apiService.Add(value);

            return Ok(_apiService.GetSingle(i => i.Id == value.Id));
        }


        [HttpPut("{id}")]
		public IActionResult Put([FromBody]LaunchpadDataRequest value)
        {
            if (!value.HadData) return BadRequest();
            _apiService.Update(value);

            return Ok(_apiService.GetSingle(i => i.Id == value.Id));
        }


        [HttpDelete("{id}")]
        public IActionResult Delete(LaunchpadDataRequest value)
        {
            if (!value.HadData) return BadRequest("Bad id provided!");
            _apiService.Delete(value);

            return Ok();
        }

    }
}