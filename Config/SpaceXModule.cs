﻿using Autofac;
using SpaceX.Web.API.Business;
using SpaceX.Web.API.Business.Interfaces;

namespace SpaceX.Web.API.Config
{
    public class SpaceXModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // Uncomment below two lines to use Database. Make sure you correct connection string in applicationSettings.json
            //builder.RegisterType<SpaceXContext>().InstancePerLifetimeScope();
           //builder.RegisterType<SpaceXDBService>().As<ISpaceXService>().InstancePerLifetimeScope();

            // Utlize SpaceX API
            builder.RegisterType<SpaceXAPIService>().As<ISpaceXService>().InstancePerLifetimeScope();
        }
    }
}
