## Getting Started 


## Switch to SpaceX API

1. Open `SpaceXModule.cs` located at `spacex.web.api\Extensions`
2. Uncomment line #18
3. Make sure everything else inside <code>protected override void Load(ContainerBuilder builder)</code> function.

You should only have below code

```
builder.RegisterType<SpaceXAPIService>().As<ISpaceXService>().InstancePerLifetimeScope();
```


## Switch to Database

1. Open `SpaceXModule.cs` located at `spacex.web.api\Extensions`
2. Uncomment lines #14 and #15.
3. Make sure everything else inside <code>protected override void Load(ContainerBuilder builder)</code> function.

You should only have below code

```
builder.RegisterType<SpaceXContext>().InstancePerLifetimeScope();
builder.RegisterType<SpaceXDBService>().As<ISpaceXService>().InstancePerLifetimeScope();
```

This project also has Swagger so you can test API endpoint(s) easily. 
Run the project in Visual Studio and you can test it at http://localhost:63708/swagger/ 