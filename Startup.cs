﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using NLog.Web;
using SpaceX.Web.API.Business;
using SpaceX.Web.API.Business.Interfaces;
using SpaceX.Web.API.Business.Models;
using SpaceX.Web.API.Business.Services;
using SpaceX.Web.API.Config;
using Swashbuckle.AspNetCore.Swagger;
using System;

namespace SpaceX.Web.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public IContainer ApplicationContainer { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            //Register configuration
            services.AddSingleton<IConfiguration>(Configuration);

            //services.AddDbContext<SpaceXContext>(options => options.UseSqlServer(connection));


            // Register the Swagger generator, defining one or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "SpaceX API", Version = "v1" });
            });

            services.AddMvc(options =>
            {
                // Force application to produce JSON response only. 
                options.Filters.Add(new ProducesAttribute("application/json"));
            });

            //Autofac is used to register services
            var builder = new ContainerBuilder();
            builder.Populate(services);
            builder.RegisterModule(new SpaceXModule());
            ApplicationContainer = builder.Build();

            return new AutofacServiceProvider(this.ApplicationContainer);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //add NLog to .NET Core
            loggerFactory.AddNLog();
            
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();
            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });
            
            app.UseMvc();
        }
    }
}
